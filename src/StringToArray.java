public class StringToArray {
    public static void main(String args[]){
        String s = "HelloWorld";
        char ch[]=s.toCharArray();
        int[]a =new int[ch.length];
        int visited=-1;
        for(int i=0;i<ch.length;i++){
            int count=1;
            for(int j=i+1;j<ch.length;j++){
                if(ch[i]==ch[j]){
                    count++;
                    a[j]=visited;
                }
            }
            if(a[i]!=visited){
                a[i]=count;
            }
        }
        System.out.println("Elements & Frequency");
        for(int k=0;k<ch.length;k++){
            if(a[k]!=visited){
                System.out.println("  " +ch[k]+ "  |  "+a[k]);
            }
        }
    }
}
