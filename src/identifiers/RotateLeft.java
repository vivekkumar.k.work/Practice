package identifiers;

import java.util.Scanner;

public class RotateLeft {
    public static void main(String[] args) {
        RotateLeft rl = new RotateLeft();
        //Initialize array
        Scanner sc = new Scanner(System.in);
        System.out.print("Please enter the size of an array");
        int arr[]= new int[sc.nextInt()];
        for(int i =0;i<arr.length;i++){
            arr[i]= sc.nextInt();
        }
        System.out.print("Please enter the number of values to rotate");
        int n =sc.nextInt();
        rl.rotateLeftMethod(arr,n);
       /* int [] a1 = new int [] {1, 2, 3, 4, 5};
        int n = 3;
        System.out.println("-----------------------");
        rl.rotateLeftMethod(a1,n);
        int [] a12 = new int [] {1, 2, 3, 4, 5};
        int n1 = 2;
        System.out.println("-----------------------");
        rl.rotateLeftMethod(a12,n1);
        int [] a13 = new int [] {1, 2, 3, 4, 5};
        int n3 = 4;
        System.out.println("-----------------------");
        rl.rotateLeftMethod(a13,n3);
        int [] a14 = new int [] {1, 2, 3, 4, 5};
        int n4 = 1;
        System.out.println("-----------------------");
        rl.rotateLeftMethod(a14,n4);*/


    }
    public static void print(int a[]){
        for(int i =0;i<a.length;i++){
            System.out.print(a[i]);
        }
    }
    public void rotateLeftMethod(int arr[],int numb){
        for(int i =0;i<numb;i++){
            int j,first;
            first =arr[0];
            for(j=0;j<arr.length-1;j++){
                arr[j] =arr[j+1];
            }
            arr[j] = first;
        }
        //String s = "hi";
        print(arr);
    }


}
