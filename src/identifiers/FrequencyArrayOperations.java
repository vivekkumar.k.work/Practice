package identifiers;

import java.util.Scanner;

public class FrequencyArrayOperations {
    public void stringToChar(String str){
        char[] ch =str.toCharArray();
        Object[] obj = new Object[ch.length];
        for (int i = 0; i < obj.length; i++) {
            obj[i] = ch[i];
        }
        System.out.println("Repeated number of Character :");
        arrayToCount(obj);

    }
    public void stringArrayToWord(String strArray[]){
        Object[] obj = new Object[strArray.length];
        for (int i = 0; i < obj.length; i++) {
            obj[i] = strArray[i];
        }
        System.out.println("Repeated number of Words :");
        arrayToCount(obj);

    }
    public void intArray(int arr[]){
        Object[] obj = new Object[arr.length];
        for (int i = 0; i < obj.length; i++) {
            obj[i] = arr[i];
        }
        System.out.println("Repeated number of Integers :");
        arrayToCount(obj);

    }
    public static void arrayToCount(Object obj[]){
        int countArray[]=new int[obj.length];
        int visited=-1;
        for(int i=0;i<obj.length;i++){
            int count=1;
            for(int j=i+1;j<obj.length;j++){
                if(obj[i] instanceof String){
                    if(obj[i].equals(obj[j])){
                        count++;
                        countArray[j]=visited;
                    }
                }else if(obj[i]==obj[j]){
                    count++;
                    countArray[j]=visited;
                }
            }
            if(countArray[i]!=visited)
                countArray[i]=count;
        }
        printArray(obj, countArray);

    }
    public static void printArray(Object obj[], int countArray[]){
        int visited=-1;
        for(int i=0;i<countArray.length;i++){
            if(countArray[i]!=visited)
                System.out.println(" " +obj[i]+ "      |      " +countArray[i]);
        }
    }
    public static void repeatOptions(Scanner sc, int option){
        FrequencyArrayOperations f = new FrequencyArrayOperations();


        if(option == 1){
            System.out.println("Please enter String:");
            sc.nextLine();
            String s =sc.nextLine();
            f.stringToChar(s);
        } else if(option ==2){
            System.out.println("Please enter IntegerArray:");
            System.out.println("Please declare size of IntegerArray:");
            int [] intValues = new int[sc.nextInt()];
            for(int i =0;i<intValues.length;i++){
                intValues[i] = sc.nextInt();
            }
            f.intArray(intValues);
        } else if(option ==3){
            System.out.println("Please enter StringArray:");
            System.out.println("Please declare size of StringArray:");
            String [] stringValues = new String[sc.nextInt()];
            sc.nextLine();
            for(int i =0;i<stringValues.length;i++){
                stringValues[i] = sc.nextLine();
            }
            f.stringArrayToWord(stringValues);
        }
    }
    public static void index(){
        System.out.println("Please select one of the option from the following input :");
        System.out.println("1. String");
        System.out.println("2. IntegerArray");
        System.out.println("3. StringArray");
        System.out.println("4. Repeat");
        System.out.println("5. Exit");
    }
    public static void main(String args[]){
        index();
        System.out.println("Enter you option :");
        Scanner sc = new Scanner(System.in);
        int option = sc.nextInt();

        while(option!=5 && option<5){
            repeatOptions(sc,option);
            index();
            option = sc.nextInt();
        }
         if(option == 5){
             System.out.println("Exit.......");
        }
        else{
            System.out.println("please select only above options. Invalid Entry");
        }
        sc.nextLine();
       
    }
}
