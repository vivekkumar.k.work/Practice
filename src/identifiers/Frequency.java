package identifiers;

public class Frequency {
   public static void main(String[] args){
       int a[]=new int[]{1,3,5,8,2,3,3,2,1};
       int f[]=new int[a.length];
       int visited=-1;
       for(int i=0;i<a.length;i++){
           int count=1;
           for(int j=i+1;j<a.length;j++){
               if(a[i]==a[j]){
                   count++;
                   f[j]=visited;
               }
           }
           if(f[i]!=visited)
               f[i]=count;
       }
       System.out.println("------------------");
       System.out.println("ELements | Frequency");
       System.out.println("-------------------");
       for(int i=0;i<f.length;i++){
           if(f[i]!=visited)
               System.out.println(" " +a[i]+ "      |      " +f[i]);
       }
       System.out.println("---------------------");
   }
}
